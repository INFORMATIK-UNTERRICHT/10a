public class GroßgehegeAquarium{
    // Attribute -> GLOBALE VARIABLEN, gelten in allen Methoden
    // Zum Verwalten einer großer Menge an gleichartigen Objekten
    // benutzen wir ein sogenanntes Array (Feld)
    private Krokodil[] herde; //-> DEKLARATION
    private int anzahlTiereInDerHerde;
    // Attribute Ende
    // --------------------------
    // Konstruktor(en)
    public GroßgehegeAquarium(){
        this.herde = new Krokodil[10];
        // Das Array bekommt 10 Fächer
        // -> INITIALISIERUNG
        this.anzahlTiereInDerHerde = 0;
    }
    // Konstruktoren Ende
    // --------------------------
    // Methode(n)
    public void KrokodilZurHerdeHinzufuegen(Krokodil neuesKrokilPar){
        if(this.anzahlTiereInDerHerde < 10){
            this.herde[this.anzahlTiereInDerHerde] = neuesKrokilPar;
            this.anzahlTiereInDerHerde++;
        }
        else{
            System.out.println("Es ist kein Platz mehr in der Herde frei!");
        }
    } 

    public void KrokodilZurHerdeHinzufuegen(Krokodil neuesKrokodilPar, int platznummerPar){
        // Es wird hier erst überprüft, ob das gesamte Array schon belegt,
        // und ob das angegebene Fach (platznummerPar) überhaupt noch frei ist
        if((this.anzahlTiereInDerHerde < 10) && (herde[platznummerPar] == null)){
            this.herde[platznummerPar] = neuesKrokodilPar;
            this.anzahlTiereInDerHerde++;
        }
        else{
            System.out.println("Es ist kein Platz mehr in der Herde frei!");
            System.out.println("Das gewünschte Fach ist NICHT mehr leer!");
        }
    }
    
        
    public void KrokodilZurHerdeHinzufuegen(String geschlechtPar){
        Krokodil KrokodilLokal = new Krokodil(geschlechtPar);// Erzeugung einer lokalen Variable
        this.KrokodilZurHerdeHinzufuegen(KrokodilLokal);
        // -> Alle lokalen Variablen werden gelöscht
    }
    
    // Aufgabe: Erweitere um eine Methode, bei der die Lebensenergie und die Anzahl gefressener Tiere
    // für das hinzuzifügende Krokodil festgelegt werden kann
    // Übertragen die Methoden auf GroßgehegSafari
    public void KrokodilZurHerdeHinzufuegen(int lebensenergiePar, int anzahlGefressenerTierePar){
        Krokodil KrokodilLokal = new Krokodil(lebensenergiePar,anzahlGefressenerTierePar);// Erzeugung einer lokalen Variable
        this.KrokodilZurHerdeHinzufuegen(KrokodilLokal);
        // -> Alle lokalen Variablen werden gelöscht
    }
    
    public void KrokodilZurHerdeHinzufuegen(){
        Krokodil KrokodilLokal = new Krokodil();// Erzeugung einer lokalen Variable
        this.KrokodilZurHerdeHinzufuegen(KrokodilLokal);
        // -> Alle lokalen Variablen werden gelöscht
    }


    public void gibInformationenAllerKrokodileAus(){
        for(int i = 0 ; i < this.herde.length ; i++){
            // Hier wird erst überprüft, ob das aktuelle Arryfach belegt ist (ungleich null)
            // nur dann wird die Botschaft gibInformationenAus() an das Objekt dieses Faches
            // geschickt.
            if(this.herde[i] != null){
                this.herde[i].gibInformationenAus();
            }
        }
    }
    // Methoden Ende
}
