import javakarol.Welt;

public class MeineWeltKlasse extends Welt{
    // Attribute
    
    // Attribute Ende
    // --------------------------------
    // Konstruktoren
    public MeineWeltKlasse(int breitePar, int laengePar, int hoehePar){
        super(breitePar, laengePar, hoehePar);
    }
    
    // Standardkonstruktor mit  Laenge = Breite = 10 und Hoehe = 5
    public MeineWeltKlasse(){
        super(10,10,5);
    }
    // Konstruktoren Ende
    // --------------------------------
    // Methoden
    
    // Methoden Ende  
}
