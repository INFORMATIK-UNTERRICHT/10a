public class GroßgehegeSafari{ 
    // Attributen 
    // Zum Verwalten einer großer Menge an gleichartigen Objekten 
    // benutzen wir ein sogenanntes Array (Feld) 
    private Giraffe[] herde; 
    private int anzahlTiereInDerHerdeG; 
    // Attributen Ende 
    // -------------------------- 
    // Konstruktor(en) 
    public GroßgehegeSafari(){ 
        this.herde = new Giraffe[10];// Das Array bekommt 10 Fächer 
        this.anzahlTiereInDerHerdeG = 0; 
    }
    // Konstruktoren Ende 
    // -------------------------- 
    // Methode(n) 
    public void giraffeZurHerdeHinzufuegen(Giraffe neueGiraffePar){ 
        if(this.anzahlTiereInDerHerdeG < 10){ 
            this.herde[this.anzahlTiereInDerHerdeG] = neueGiraffePar; 
            this.anzahlTiereInDerHerdeG++; 
        } 
        else{ 
            System.out.println("Es ist kein Platz mehr in der Herde frei!"); 
        } 
    } 

    public void giraffeZurHerdeHinzufuegen(Giraffe neueGiraffePar, int platznummerPar){ 
        // Es wird hier erst überprüft, ob das gesamte Array schon belegt, 
        // und ob das angegebene Fach (platznummerPar) überhaupt noch frei ist 
        if((this.anzahlTiereInDerHerdeG < 10) && (herde[platznummerPar] == null)){ 
            this.herde[platznummerPar] = neueGiraffePar; 
            this.anzahlTiereInDerHerdeG++; 
        } 
        else{ 
            System.out.println("Es ist kein Platz mehr in der Herde frei!"); 
            System.out.println("Das gewünschte Fach ist NICHT mehr leer!"); 
        } 
    } 
    
    public void giraffeZurHerdeHinzufuegen(){ 
            Giraffe neueGiraffeLok = new Giraffe();
            this.giraffeZurHerdeHinzufuegen(neueGiraffeLok);
    }
    
    public void giraffeZurHerdeHinzufuegen(String geschlechtPar){ 
            Giraffe neueGiraffeLok = new Giraffe(geschlechtPar);
            this.giraffeZurHerdeHinzufuegen(neueGiraffeLok);
    }

    public void gibInformationenAllerGiraffenAus(){ 
        for(int i = 0 ; i < this.herde.length ; i++){
            // Hier wird erst überprüft, ob das aktuelle Arryfach belegt ist (ungleich null) 
            // nur dann wird die Botschaft gibInformationenAus() an das Objekt dieses Faches 
            // geschickt. 
            if(this.herde[i] != null){ 
                this.herde[i].gibInformationenAus(); 
            } 
        } 
    } 
    // Methoden Ende 
} 
