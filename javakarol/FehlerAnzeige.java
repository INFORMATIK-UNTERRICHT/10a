package javakarol;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;

public class FehlerAnzeige extends JTextArea {
  public FehlerAnzeige() {
    setBackground(new Color(224, 224, 224));
    setEnabled(false);
    setDisabledTextColor(Color.BLACK);
    setFont(new Font("Arial", 0, 12));
    setText("");
  }
  
  public void setLineText(int zeile, String s) {
    int zeilenAnz = getLineCount();
    if (zeile > zeilenAnz) {
      for (int i = 1; i < zeile - zeilenAnz; i++)
        append("\n"); 
      append(s);
    } else {
      try {
        replaceRange(s, getLineStartOffset(zeile - 1), getLineEndOffset(zeile - 1));
      } catch (BadLocationException badLocationException) {}
    } 
  }
  
  public String getLineText(int zeile) {
    String ergeb = "";
    int zeilenAnz = getLineCount();
    if (zeile > zeilenAnz) {
      ergeb = "";
    } else {
      try {
        int zeileStart = getLineStartOffset(zeile - 1);
        ergeb = getText(zeileStart, getLineEndOffset(zeile - 1) - zeileStart);
      } catch (BadLocationException badLocationException) {}
    } 
    return ergeb;
  }
}
