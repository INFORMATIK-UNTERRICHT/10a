package javakarol;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class WeltAnzeige2D extends JPanel {
  private Welt welt;
  
  private GraphicsConfiguration gfxConf;
  
  private BufferedImage zeichenFlaeche;
  
  private Point weltOrigin2D = new Point(0, 0);
  
  private Graphics2D gDC;
  
  private Color hintergrundFarbe = Color.WHITE;
  
  private int[] fuellFarbe = new int[] { 13434880, 14474240, 205, 52480, 3881787 };
  
  private int[] randFarbe = new int[] { 16711680, 16776960, 255, 65280, 2105376 };
  
  private final int randLinks = 20;
  
  private final int randOben = 20;
  
  private final int randUnten = 10;
  
  private final int fliese = 30;
  
  public WeltAnzeige2D(Welt welt) {
    this.welt = welt;
    this.gfxConf = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
    zeichenFlaecheVorbereiten();
    setBackground(Color.WHITE);
    setBorder(BorderFactory.createLineBorder(Color.green));
    setOpaque(true);
    setPreferredSize(new Dimension(getMinBreite(), getMinHoehe()));
  }
  
  public int getMinBreite() {
    return this.zeichenFlaeche.getWidth() + 10;
  }
  
  public int getMinHoehe() {
    return this.zeichenFlaeche.getHeight() + 10;
  }
  
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.drawImage(this.zeichenFlaeche, 0, 0, null);
  }
  
  public void paintToFile(File f, String fileTyp) {
    try {
      ImageIO.write(this.zeichenFlaeche, fileTyp, f);
    } catch (IOException iOException) {}
  }
  
  protected BufferedImage getZeichenflaeche() {
    return this.zeichenFlaeche;
  }
  
  protected void zeichneWelt() {
    int links = Integer.MAX_VALUE;
    int oben = Integer.MAX_VALUE;
    int rechts = Integer.MIN_VALUE;
    int unten = Integer.MIN_VALUE;
    for (int a = 1; a <= this.welt.areaBreite; a++) {
      for (int b = 1; b <= this.welt.areaLaenge; b++) {
        boolean inv = false;
        for (int c = 0; c < this.welt.areaHoehe; c++) {
          if (this.welt.areaStapelInvalid[a][b][c]) {
            inv = true;
            this.welt.areaStapelInvalid[a][b][c] = false;
          } 
        } 
        if (inv) {
          if (links > a)
            links = a; 
          if (oben > b)
            oben = b; 
          if (rechts < a)
            rechts = a; 
          if (unten < b)
            unten = b; 
        } 
      } 
    } 
    if (links != Integer.MAX_VALUE && links != Integer.MIN_VALUE) {
      links = Math.min(Math.max(1, links), this.welt.areaBreite);
      oben = Math.min(Math.max(1, oben), this.welt.areaLaenge);
      rechts = Math.min(Math.max(1, rechts), this.welt.areaBreite);
      unten = Math.min(Math.max(0, unten), this.welt.areaLaenge);
      zeichenWeltRechteck(links, oben, rechts, unten);
      revalidate();
      repaint();
    } 
  }
  
  protected void zeichneWeltGanz() {
    loescheZeichenflaeche();
    zeichenWeltRechteck(1, 1, this.welt.areaBreite, this.welt.areaLaenge);
    for (int a = 1; a <= this.welt.areaBreite; a++) {
      for (int b = 1; b <= this.welt.areaLaenge; b++) {
        for (int c = 0; c < this.welt.areaHoehe; c++)
          this.welt.areaStapelInvalid[a][b][c] = false; 
      } 
    } 
    revalidate();
    repaint();
  }
  
  protected void resetWeltAnzeige() {
    loescheZeichenflaeche();
    paintImmediately(0, 0, getMinBreite(), getMinHoehe());
    this.zeichenFlaeche.flush();
    this.gDC = null;
    zeichenFlaecheVorbereiten();
    setBackground(Color.WHITE);
    setPreferredSize(new Dimension(getMinBreite(), getMinHoehe()));
    revalidate();
    repaint();
  }
  
  private void loescheZeichenflaeche() {
    Color aktuell = this.gDC.getColor();
    this.gDC.setColor(this.hintergrundFarbe);
    this.gDC.fillRect(0, 0, this.zeichenFlaeche.getWidth(), this.zeichenFlaeche.getHeight());
    this.gDC.setColor(aktuell);
  }
  
  private void zeichenFlaecheVorbereiten() {
    Point pp2 = new Point(0, 0);
    Dimension ergeb = new Dimension(0, 0);
    this.weltOrigin2D.x = 20;
    this.weltOrigin2D.y = 20;
    Point pp1 = p2(0, 0);
    pp2 = p2(this.welt.areaBreite, this.welt.areaLaenge);
    ergeb.width = Math.abs(pp1.x - pp2.x) + 40;
    ergeb.height = Math.abs(pp1.y - pp2.y) + 20 + 10;
    this.zeichenFlaeche = this.gfxConf.createCompatibleImage(ergeb.width, ergeb.height);
    this.gDC = this.zeichenFlaeche.createGraphics();
    loescheZeichenflaeche();
  }
  
  private void zeichenWeltRechteck(int links, int oben, int rechts, int unten) {
    Point pp2 = new Point(0, 0);
    int[] polyx = new int[3];
    int[] polyy = new int[3];
    String t = "";
    FontMetrics fm = null;
    Roboter robo = null;
    Point pp1 = p2(links - 1, oben - 1);
    pp2 = p2(rechts, unten);
    this.gDC.setColor(this.hintergrundFarbe);
    this.gDC.fillRect(pp1.x, pp1.y, pp2.x - pp1.x, pp2.y - pp1.y);
    this.gDC.setColor(Color.BLUE);
    for (int i = links; i <= rechts + 1; i++) {
      pp1 = p2(i - 1, oben - 1);
      pp2 = p2(i - 1, unten);
      this.gDC.drawLine(pp1.x, pp1.y, pp2.x, pp2.y);
    } 
    for (int b = oben; b <= unten + 1; b++) {
      pp1 = p2(links - 1, b - 1);
      pp2 = p2(rechts, b - 1);
      this.gDC.drawLine(pp1.x, pp1.y, pp2.x, pp2.y);
    } 
    for (int a = links; a <= rechts; a++) {
      for (int j = oben; j <= unten; j++) {
        pp1 = p2(a - 1, j - 1);
        pp1.x += 30;
        pp1.y += 30;
        byte part = this.welt.getPart(a, j, 0);
        this.welt.getClass();
        this.welt.getClass();
        this.welt.getClass();
        if (part == 1 || (part >= 11 && part <= 14)) {
          byte ziegelNr, parto = this.welt.getPart(a, j, this.welt.areaStapelHoehe[a][j] - 1);
          this.welt.getClass();
          if (parto == 1) {
            this.welt.getClass();
            ziegelNr = 11;
          } else {
            this.welt.getClass();
            ziegelNr = (byte)(parto - 11);
          } 
          this.gDC.setColor(new Color(this.fuellFarbe[ziegelNr]));
          this.gDC.fillRect(pp1.x + 1, pp1.y + 1, 28, 28);
          this.gDC.setColor(new Color(this.randFarbe[ziegelNr]));
          this.gDC.drawRect(pp1.x + 1, pp1.y + 1, 28, 28);
          t = Integer.toString(this.welt.brickCount(a, j));
          this.gDC.setFont(new Font("Arial", 0, 15));
          fm = this.gDC.getFontMetrics();
          int tw = (30 - fm.stringWidth(t)) / 2;
          int th = (30 + fm.getAscent()) / 2;
          this.gDC.setColor(Color.WHITE);
          this.gDC.drawString(t, (pp1.x + tw), (pp1.y + th));
        } 
        this.welt.getClass();
        if (part == 2) {
          this.gDC.setColor(new Color(4210752));
          this.gDC.fillRect(pp1.x + 1, pp1.y + 1, 28, 28);
          this.gDC.setColor(new Color(8421504));
          this.gDC.drawRect(pp1.x + 1, pp1.y + 1, 28, 28);
        } 
        if (this.welt.isMarker(a, j)) {
          byte markeNr, mark = this.welt.getMarker(a, j);
          this.welt.getClass();
          if (mark == 4) {
            this.welt.getClass();
            markeNr = 22;
          } else {
            this.welt.getClass();
            markeNr = (byte)(mark - 21);
          } 
          this.gDC.setColor(new Color(this.randFarbe[markeNr]));
          this.gDC.fillRect(pp1.x + 1, pp1.y + 1, 28, 28);
          this.gDC.setColor(Color.BLACK);
          this.gDC.drawRect(pp1.x + 1, pp1.y + 1, 28, 28);
        } 
        int rpos = this.welt.getRobotIndex(a, j);
        if (rpos >= 0) {
          robo = (Roboter)this.welt.alleRoboter.get(rpos);
          switch (robo.BlickrichtungGeben()) {
            case 'S':
              polyx[0] = pp1.x + 1;
              polyy[0] = pp1.y + 1;
              polyx[1] = pp2.x - 1;
              polyy[1] = pp1.y + 1;
              polyx[2] = pp1.x + 15;
              polyy[2] = pp2.y - 1;
              break;
            case 'O':
              polyx[0] = pp1.x + 1;
              polyy[0] = pp2.y - 1;
              polyx[1] = pp1.x + 1;
              polyy[1] = pp1.y + 1;
              polyx[2] = pp2.x - 1;
              polyy[2] = pp1.y + 15;
              break;
            case 'N':
              polyx[0] = pp2.x - 1;
              polyy[0] = pp2.y - 1;
              polyx[1] = pp1.x + 1;
              polyy[1] = pp2.y - 1;
              polyx[2] = pp1.x + 15;
              polyy[2] = pp1.y + 1;
              break;
            case 'W':
              polyx[0] = pp2.x - 1;
              polyy[0] = pp1.y + 1;
              polyx[1] = pp2.x - 1;
              polyy[1] = pp2.y - 1;
              polyx[2] = pp1.x + 1;
              polyy[2] = pp1.y + 15;
              break;
          } 
          this.gDC.setColor(Color.BLACK);
          this.gDC.fill(new Polygon(polyx, polyy, 3));
          if (this.welt.alleRoboter.size() > 1) {
            t = Integer.toString(robo.KennungGeben());
            this.gDC.setFont(new Font("Arial", 0, 15));
            fm = this.gDC.getFontMetrics();
            int tw = (30 - fm.stringWidth(t)) / 2;
            int th = (30 + fm.getAscent()) / 2;
            this.gDC.setColor(Color.WHITE);
            this.gDC.drawString(t, (pp1.x + tw), (pp1.y + th));
          } 
        } 
      } 
    } 
  }
  
  private Point p2(int a, int b) {
    Point ergeb = new Point();
    a = Math.abs(a);
    b = Math.abs(b);
    ergeb.x = Math.round((this.weltOrigin2D.x + 30 * a));
    ergeb.y = Math.round((this.weltOrigin2D.y + 30 * b));
    return ergeb;
  }
  
  protected Point p2ToWelt(int x, int y) {
    Point ergeb = new Point();
    ergeb.x = (int)Math.floor(((x - this.weltOrigin2D.x) / 30.0F)) + 1;
    ergeb.y = (int)Math.floor(((y - this.weltOrigin2D.y) / 30.0F)) + 1;
    return ergeb;
  }
}
