package javakarol;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class WeltAnzeige3D extends JPanel {
  private Welt welt;
  
  private GraphicsConfiguration gfxConf;
  
  private BufferedImage zeichenFlaeche;
  
  private Point weltOrigin3D = new Point(0, 0);
  
  private Graphics2D gDC;
  
  protected BufferedImage quaderImg = null;
  
  protected BufferedImage[] ziegelImg = new BufferedImage[4];
  
  protected BufferedImage[] markeImg = new BufferedImage[5];
  
  private final int maxRobotImages = 9;
  
  protected BufferedImage[][] karolImg = new BufferedImage[4][9];
  
  private BufferedImage weltFlaeche = null;
  
  protected int maxImageHoehe = 10;
  
  private Color hintergrundFarbe = Color.WHITE;
  
  private final int randLinks = 40;
  
  private final int randOben = 40;
  
  private final int randUnten = 30;
  
  private final int weltOben = 20;
  
  private int zoomWert = 0;
  
  private double[] zoomFaktor = new double[] { 0.6D, 0.7D, 0.8D, 1.0D, 1.5D, 2.0D, 2.5D };
  
  public WeltAnzeige3D(Welt welt) {
    this.welt = welt;
    this.gfxConf = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
    zeichenFlaecheVorbereiten();
    weltFlaecheVorbereiten();
    imagesLaden();
    setBackground(Color.WHITE);
    setBorder(BorderFactory.createLineBorder(Color.red));
    setOpaque(true);
    this.zoomWert = 0;
    setPreferredSize(new Dimension(getMinBreite(), getMinHoehe()));
  }
  
  public int getMinBreite() {
    if (this.zoomWert == 0)
      return this.zeichenFlaeche.getWidth() + 10; 
    return (int)(this.zoomFaktor[this.zoomWert + 3] * this.zeichenFlaeche.getWidth() + 10.0D);
  }
  
  public int getMinHoehe() {
    if (this.zoomWert == 0)
      return this.zeichenFlaeche.getHeight() + 10; 
    return (int)(this.zoomFaktor[this.zoomWert + 3] * this.zeichenFlaeche.getHeight() + 10.0D);
  }
  
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    if (this.zoomWert == 0) {
      g.drawImage(this.zeichenFlaeche, 0, 0, null);
    } else {
      Graphics2D g2 = (Graphics2D)g;
      g2.scale(this.zoomFaktor[this.zoomWert + 3], this.zoomFaktor[this.zoomWert + 3]);
      g2.drawImage(this.zeichenFlaeche, 0, 0, (ImageObserver)null);
    } 
  }
  
  public void paintToFile(File f, String fileTyp) {
    try {
      ImageIO.write(this.zeichenFlaeche, fileTyp, f);
    } catch (IOException iOException) {}
  }
  
  protected BufferedImage getZeichenflaeche() {
    return this.zeichenFlaeche;
  }
  
  private void loescheZeichenflaeche() {
    Color aktuell = this.gDC.getColor();
    this.gDC.setColor(this.hintergrundFarbe);
    this.gDC.fillRect(0, 0, this.zeichenFlaeche.getWidth(), this.zeichenFlaeche.getHeight());
    this.gDC.setColor(aktuell);
  }
  
  protected void zeichneWelt() {
    int links = Integer.MAX_VALUE;
    int oben = Integer.MAX_VALUE;
    int rechts = Integer.MIN_VALUE;
    int unten = Integer.MIN_VALUE;
    Point p1 = new Point(0, 0);
    Point p2 = new Point(0, 0);
    for (int a = 1; a <= this.welt.areaBreite; a++) {
      for (int b = 1; b <= this.welt.areaLaenge; b++) {
        for (int c = 0; c < this.welt.areaHoehe; c++) {
          if (this.welt.areaStapelInvalid[a][b][c]) {
            p1 = p3((a - 1), b, c);
            p2 = p3(a, (b - 1), c);
            if (links > p1.x)
              links = p1.x; 
            if (oben > p2.y - this.maxImageHoehe)
              oben = p2.y - this.maxImageHoehe; 
            if (rechts < p2.x)
              rechts = p2.x; 
            if (unten < p1.y)
              unten = p1.y; 
            this.welt.areaStapelInvalid[a][b][c] = false;
          } 
        } 
      } 
    } 
    if (links != Integer.MAX_VALUE && links != Integer.MIN_VALUE) {
      links = Math.max(0, links - 10);
      oben = Math.max(0, oben - 10);
      rechts = Math.min(this.weltFlaeche.getWidth(), rechts + 10);
      unten = Math.min(this.weltFlaeche.getHeight(), unten + 10);
      zeichenWeltRechteck(links, oben, rechts, unten);
      revalidate();
      repaint();
    } 
  }
  
  protected void zeichneWeltGanz() {
    loescheZeichenflaeche();
    zeichenWeltRechteck(0, 0, this.weltFlaeche.getWidth(), this.weltFlaeche.getHeight());
    for (int a = 1; a <= this.welt.areaBreite; a++) {
      for (int b = 1; b <= this.welt.areaLaenge; b++) {
        for (int c = 0; c < this.welt.areaHoehe; c++)
          this.welt.areaStapelInvalid[a][b][c] = false; 
      } 
    } 
    revalidate();
    repaint();
  }
  
  private void zeichenWeltRechteck(int x1, int y1, int x2, int y2) {
    Point p1 = new Point(0, 0);
    Rectangle clipRect = new Rectangle(x1, y1, x2 - x1, y2 - y1);
    this.gDC.setClip(x1 + 40, y1 + 40, x2 - x1, y2 - y1);
    this.gDC.drawImage(this.weltFlaeche, x1 + 40, y1 + 40, x2 + 40, y2 + 40, x1, y1, x2, y2, this);
    for (int a = 1; a <= this.welt.areaBreite; a++) {
      for (int b = 1; b <= this.welt.areaLaenge; b++) {
        for (int c = 0; c <= this.welt.areaStapelHoehe[a][b]; c++) {
          byte part = this.welt.getPart(a, b, c);
          this.welt.getClass();
          this.welt.getClass();
          this.welt.getClass();
          if (part == 1 || (part >= 11 && part <= 14)) {
            byte ziegelNr;
            this.welt.getClass();
            if (part == 1) {
              this.welt.getClass();
              ziegelNr = 11;
            } else {
              this.welt.getClass();
              ziegelNr = (byte)(part - 11);
            } 
            p1 = p3((a - 1), b, c);
            p1.y -= this.ziegelImg[ziegelNr].getHeight();
            if (clipRect.intersects(new Rectangle(p1.x, p1.y, this.ziegelImg[ziegelNr].getWidth(), this.ziegelImg[ziegelNr].getHeight())))
              this.gDC.drawImage(this.ziegelImg[ziegelNr], p1.x + 40, p1.y + 40, this); 
          } 
          this.welt.getClass();
          if (part == 2) {
            p1 = p3((a - 1), b, 0.0F);
            p1.y -= this.quaderImg.getHeight();
            if (clipRect.intersects(new Rectangle(p1.x, p1.y, this.quaderImg.getWidth(), this.quaderImg.getHeight())))
              this.gDC.drawImage(this.quaderImg, p1.x + 40, p1.y + 40, this); 
          } 
          if (this.welt.isMarker(a, b) && this.welt.brickCount(a, b) == c) {
            byte markeNr, mark = this.welt.getMarker(a, b);
            this.welt.getClass();
            if (mark == 4) {
              this.welt.getClass();
              markeNr = 22;
            } else {
              this.welt.getClass();
              markeNr = (byte)(mark - 21);
            } 
            p1 = p3((a - 1), b, c);
            p1.y -= this.markeImg[markeNr].getHeight();
            if (clipRect.intersects(new Rectangle(p1.x, p1.y, this.markeImg[markeNr].getWidth(), this.markeImg[markeNr].getHeight())))
              this.gDC.drawImage(this.markeImg[markeNr], p1.x + 40, p1.y + 40, this); 
          } 
          int anzRoboter = this.welt.alleRoboter.size();
          for (int i = 0; i < anzRoboter; i++) {
            Roboter robo = (Roboter)this.welt.alleRoboter.get(i);
            if (a == robo.PositionXGeben() && b == robo.PositionYGeben() && c == this.welt.areaStapelHoehe[a][b] && 
              robo.SichtbarkeitGeben()) {
              p1 = p3(a - 0.9F, b - 0.3F, c);
              int richtung = robo.getBlickrichtungNr();
              int knr = Math.min(Math.max(robo.KennungGeben() - 1, 0), 8);
              p1.x -= (this.karolImg[richtung][knr].getWidth() - 30) / 2;
              p1.y -= this.karolImg[richtung][knr].getHeight();
              if (clipRect.intersects(new Rectangle(p1.x, p1.y, this.karolImg[richtung][knr].getWidth(), this.karolImg[richtung][knr].getHeight())))
                this.gDC.drawImage(this.karolImg[richtung][knr], p1.x + 40, p1.y + 40, this); 
            } 
          } 
        } 
      } 
    } 
  }
  
  private void weltFlaecheVorbereiten() {
    Point p1 = new Point(0, 0);
    Point p2 = new Point(0, 0);
    float[] dash_array = new float[4];
    dash_array[0] = 10.0F;
    dash_array[1] = 5.0F;
    dash_array[2] = 5.0F;
    dash_array[3] = 5.0F;
    BasicStroke gestrichelt = new BasicStroke(
        1.0F, 
        0, 
        2, 
        1.0F, 
        dash_array, 
        0.0F);
    BasicStroke durchgehend = new BasicStroke();
    this.weltFlaeche = this.gfxConf.createCompatibleImage(this.zeichenFlaeche.getWidth() - 80 + 1, this.zeichenFlaeche.getHeight() - 40 - 30 + 1);
    Graphics2D g = this.weltFlaeche.createGraphics();
    g.setColor(this.hintergrundFarbe);
    g.fillRect(0, 0, this.weltFlaeche.getWidth(), this.weltFlaeche.getHeight());
    g.setColor(Color.BLUE);
    g.setStroke(durchgehend);
    int i;
    for (i = 0; i <= this.welt.areaLaenge; i++) {
      p1 = p3(0.0F, i, 0.0F);
      p2 = p3(this.welt.areaBreite, i, 0.0F);
      g.drawLine(p1.x, p1.y, p2.x, p2.y);
    } 
    for (i = 0; i <= this.welt.areaBreite; i++) {
      p1 = p3(i, 0.0F, 0.0F);
      p2 = p3(i, this.welt.areaLaenge, 0.0F);
      g.drawLine(p1.x, p1.y, p2.x, p2.y);
    } 
    g.setStroke(gestrichelt);
    for (i = 0; i <= this.welt.areaBreite; i++) {
      p1 = p3(i, 0.0F, 0.0F);
      p2 = p3(i, 0.0F, this.welt.areaHoehe);
      g.drawLine(p1.x, p1.y, p2.x, p2.y);
    } 
    p1 = p3(this.welt.areaBreite, 0.0F, this.welt.areaHoehe);
    p2 = p3(0.0F, 0.0F, this.welt.areaHoehe);
    g.drawLine(p1.x, p1.y, p2.x, p2.y);
    for (i = 0; i <= this.welt.areaLaenge; i++) {
      p1 = p3(0.0F, i, 0.0F);
      p2 = p3(0.0F, i, this.welt.areaHoehe);
      g.drawLine(p1.x, p1.y, p2.x, p2.y);
    } 
    p1 = p3(0.0F, this.welt.areaLaenge, this.welt.areaHoehe);
    p2 = p3(0.0F, 0.0F, this.welt.areaHoehe);
    g.drawLine(p1.x, p1.y, p2.x, p2.y);
    g.setStroke(durchgehend);
    p1 = p3(-2.0F, 0.0F, this.welt.areaHoehe);
    p2 = p3(-2.0F, 2.0F, this.welt.areaHoehe);
    g.drawLine(p1.x, p1.y, p2.x, p2.y);
    g.drawLine(p1.x - 10, p1.y + 5, p1.x, p1.y);
    g.drawLine(p1.x - 5, p1.y + 10, p1.x, p1.y);
    g.setFont(new Font("Arial", 0, 14));
    g.drawString("N", p1.x + 4, p1.y);
  }
  
  protected BufferedImage imageLadenName(String name) {
    BufferedImage bi = null;
    URL u = ClassLoader.getSystemResource("imgs/" + name + ".gif");
    if (u == null)
      u = WeltAnzeige3D.class.getResource("imgs/" + name + ".gif"); 
    try {
      bi = ImageIO.read(u);
    } catch (IOException e) {
      System.out.println("Ein noetiges Image fZiegel/Quader/Roboter kann nicht geladen werden.");
      throw new RuntimeException("Fehler beim Laden eines Images.");
    } 
    if (bi.getHeight() > this.maxImageHoehe)
      this.maxImageHoehe = bi.getHeight(); 
    return bi;
  }
  
  protected void imagesLaden() {
    this.ziegelImg[0] = imageLadenName("Ziegel_rot");
    this.ziegelImg[1] = imageLadenName("Ziegel_gelb");
    this.ziegelImg[2] = imageLadenName("Ziegel_blau");
    this.ziegelImg[3] = imageLadenName("Ziegel_gruen");
    this.markeImg[0] = imageLadenName("Marke_rot");
    this.markeImg[1] = imageLadenName("Marke_gelb");
    this.markeImg[2] = imageLadenName("Marke_blau");
    this.markeImg[3] = imageLadenName("Marke_gruen");
    this.markeImg[4] = imageLadenName("Marke_schwarz");
    this.quaderImg = imageLadenName("Quader");
    for (int i = 1; i <= 9; i++) {
      this.karolImg[0][i - 1] = imageLadenName("robotS" + i);
      this.karolImg[1][i - 1] = imageLadenName("robotW" + i);
      this.karolImg[2][i - 1] = imageLadenName("robotN" + i);
      this.karolImg[3][i - 1] = imageLadenName("robotO" + i);
    } 
  }
  
  private Point p3(float x, float y, float z) {
    Point ergeb = new Point();
    ergeb.x = Math.round(this.weltOrigin3D.x + 30.0F * x - 15.0F * y);
    ergeb.y = Math.round(this.weltOrigin3D.y + 15.0F * y - 15.0F * z);
    return ergeb;
  }
  
  private void zeichenFlaecheVorbereiten() {
    Dimension ergeb = new Dimension(0, 0);
    Point[] punkte = new Point[8];
    int links = Integer.MAX_VALUE;
    int oben = Integer.MAX_VALUE;
    int rechts = Integer.MIN_VALUE;
    int unten = Integer.MIN_VALUE;
    punkte[0] = p3(0.0F, 0.0F, 0.0F);
    punkte[1] = p3(0.0F, 0.0F, this.welt.areaHoehe);
    punkte[2] = p3(0.0F, this.welt.areaLaenge, 0.0F);
    punkte[3] = p3(0.0F, this.welt.areaLaenge, this.welt.areaHoehe);
    punkte[4] = p3(this.welt.areaBreite, 0.0F, 0.0F);
    punkte[5] = p3(this.welt.areaBreite, 0.0F, this.welt.areaHoehe);
    punkte[6] = p3(this.welt.areaBreite, this.welt.areaLaenge, 0.0F);
    punkte[7] = p3(this.welt.areaBreite, this.welt.areaLaenge, this.welt.areaHoehe);
    for (int i = 0; i < 7; i++) {
      if ((punkte[i]).x > rechts)
        rechts = (punkte[i]).x; 
      if ((punkte[i]).y > unten)
        unten = (punkte[i]).y; 
      if ((punkte[i]).x < links)
        links = (punkte[i]).x; 
      if ((punkte[i]).y < oben)
        oben = (punkte[i]).y; 
    } 
    ergeb.width = Math.abs(rechts - links) + 80;
    ergeb.height = Math.abs(unten - oben) + 40 + 30 + 20;
    this.weltOrigin3D.x = Math.abs((punkte[0]).x - links);
    this.weltOrigin3D.y = Math.abs((punkte[0]).y - oben) + 20;
    this.zeichenFlaeche = this.gfxConf.createCompatibleImage(ergeb.width, ergeb.height);
    this.gDC = this.zeichenFlaeche.createGraphics();
    loescheZeichenflaeche();
  }
  
  protected void resetWeltAnzeige() {
    loescheZeichenflaeche();
    paintImmediately(0, 0, getMinBreite(), getMinHoehe());
    this.zoomWert = 0;
    this.zeichenFlaeche.flush();
    this.weltFlaeche.flush();
    this.gDC = null;
    zeichenFlaecheVorbereiten();
    weltFlaecheVorbereiten();
    setBackground(Color.WHITE);
    setPreferredSize(new Dimension(getMinBreite(), getMinHoehe()));
    revalidate();
    repaint();
  }
  
  public int getZoomWert() {
    return this.zoomWert;
  }
  
  private void zoomSetzen(int neuerWert) {
    if (this.zoomWert != neuerWert) {
      loescheZeichenflaeche();
      paintImmediately(0, 0, getMinBreite(), getMinHoehe());
      this.zoomWert = neuerWert;
      setBackground(Color.WHITE);
      setPreferredSize(new Dimension(getMinBreite(), getMinHoehe()));
      revalidate();
      repaint();
    } 
  }
  
  public boolean zoomen(boolean hinein) {
    int neuerWert = this.zoomWert;
    boolean ergeb = false;
    if (hinein && this.zoomWert < 3)
      neuerWert++; 
    if (!hinein && this.zoomWert > -3)
      neuerWert--; 
    if (neuerWert != this.zoomWert) {
      zoomSetzen(neuerWert);
      ergeb = true;
    } 
    return ergeb;
  }
  
  public void zoomZuruecksetzen() {
    if (this.zoomWert != 0)
      zoomSetzen(0); 
  }
}
