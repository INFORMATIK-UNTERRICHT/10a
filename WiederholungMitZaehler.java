
// Author: R. Loeffler
// Date: 22.02.2021
// Subject: Wiederholung mit Zaehler

public class WiederholungMitZaehler{
    // Attribute
        // Keine
    // Attribute Ende
    // ------------------------------
    // Konstruktor(en)
    public WiederholungMitZaehler(){
        // Leer
    }
    // Konstruktor(en) Ende
    // ------------------------------
    // Methoden
    //Aufgabe 1
    public double kapitalBerechnen(double kapitalPar,int startPar,double pPar,int zielPar){
        for (int i = startPar; i <= zielPar; i++){ // if(int i = 0 ; i < endwert ; i++){....
            kapitalPar = kapitalPar * (1+(pPar/100));
        }
        return kapitalPar;
    }

    //Aufgabe 2
    public int natuerlicheZahlen(int zielPar){
        int summeLok = 0; // lokale Variable summeLok
        for(int i=1; i<=zielPar;i++){ // Beim ersten Mal i = 1, beim zweiten Mal i = 2 ; beim dritten Mal i =3
            summeLok += i; // addiert immer die Zählvariable i zur summeLok
        }
        return summeLok;
    }

    public int quadratZahlen(int zielPar){
        int summeLok = 0;
        for(int i=0; i<=zielPar;i++){
            summeLok += i*i;
        }
        return summeLok;
    }

    public int kubikZahlen(int zielPar){
        int summeLok = 0;
        for(int i=0; i<=zielPar;i++){
            summeLok += i*i*i;
        }
        return summeLok;
    }

    public int stammBrüche(int zielPar){
        int summeLok = 0;
        for(int i=1; i <= zielPar ;i++){
            summeLok += 1/i;
        }
        return summeLok;
    }

    //Aufgabe 3
    public double gibPotenz(double basisPar , int exponentPar){
        double ergebnisLok =1;
        if (exponentPar > 0){
            for(int i=1; i<=exponentPar;i++){
                ergebnisLok = basisPar * ergebnisLok;
            }
        }
        else{
            if (exponentPar < 0){ // negative Potenzen sind gleich dem Kehrbruch mit positiven Potenzen
                for(int i=1; i<=exponentPar;i++){
                    ergebnisLok = basisPar * ergebnisLok;
                }
                ergebnisLok = 1/ ergebnisLok;
            }
            else{// Potenz gleich 0 -> Ergebnis gleich 1
                ergebnisLok = 1;
            }
        }
        return ergebnisLok;
    }

    //Aufgabe 4
    public int gibFakultaet(int nPar){
        int ergebnisLok = 1;
        for(int i=1; i<= nPar ; i++){
            ergebnisLok = ergebnisLok * i;
        }
        return ergebnisLok;    
    }
    // Methoden Ende
}