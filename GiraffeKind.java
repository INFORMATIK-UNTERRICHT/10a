public class GiraffeKind extends Giraffe{
    // Attribute
    private int anzahlKuscheltiere;
    // Attribute Ende
    // ------------------------------
    // Konstruktoren 
    public GiraffeKind(){
        // Im Hintergrund erstellt der Konstruktor ein Elternobjekt
        // und zwar mit Hilfe des Standardkonstruktors
        this.anzahlKuscheltiere = 1;
    }

    public GiraffeKind(String geschlechtPar){
        super(geschlechtPar); //Mit dem Schlüsselwort super wird der Konstruktor der Elternklasse aufgerufen
        this.anzahlKuscheltiere = 1;
        // this.geschlecht = geschlechtPar;
    }
    // Konstruktoren Ende
    // Methoden
    public void erhalteKuscheltiere(int anzahlKuscheltierePar){
        this.anzahlKuscheltiere += anzahlKuscheltierePar;
    }
    // Methoden Ende
}