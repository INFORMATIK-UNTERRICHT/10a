import javakarol.Roboter;
import javakarol.Welt;

class MeineRoboterKlasse extends Roboter{
    // Attribute
    
    // Attribute Ende
    // --------------------------------
    // Konstruktoren
    public MeineRoboterKlasse(Welt weltPar){
        super(weltPar);
    }
    // Konstruktoren Ende
    // --------------------------------
    // Methoden
    public void umdrehen(){
        this.LinksDrehen();
        this.LinksDrehen();
    }
    // Methoden Ende    
}