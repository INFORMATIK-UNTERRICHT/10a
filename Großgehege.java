public class Großgehege{
    private Tier[] herde; 
    private int anzahlTiereInDerHerde;
    // Attribute Ende
    // --------------------------
    // Konstruktor(en)
    public Großgehege(){
        this.herde = new Tier[10];
        // Das Array bekommt 10 Fächer
        // -> INITIALISIERUNG
        this.anzahlTiereInDerHerde = 0;
    }
    // Konstruktoren Ende
    // --------------------------
    // Methode(n)
    public void tierHinzufuegen(Tier tierPar){
        if(this.anzahlTiereInDerHerde < 10){
            this.herde[this.anzahlTiereInDerHerde] = tierPar;
            this.anzahlTiereInDerHerde++;
        }
        else{
            System.out.println("Es ist kein Platz mehr in der Herde frei!");
        }
    } 
    
    public void informationenAusgeben(){
        for(int i = 0 ; i < this.anzahlTiereInDerHerde ; i++){
            this.herde[i].gibInformationenAus();
        }
    }
    
    
}