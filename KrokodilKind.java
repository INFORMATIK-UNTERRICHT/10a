public class KrokodilKind extends Krokodil{  
    // Attribute   
    private int anzahlSeerosenAlsMakeUp; 
    // Attribute Ende  
    // ------------------------------  
    // Konstruktoren   
    public KrokodilKind(){  
        // Im Hintergrund erstellt der Konstruktor ein Elternobjekt  
        // und zwar mit Hilfe des Standardkonstruktors  
        this.anzahlSeerosenAlsMakeUp = 1;  
    }  

    public KrokodilKind(String geschlechtPar){  
        super(geschlechtPar); //Mit dem Schlüsselwort super wird der Konstruktor der Elternklasse aufgerufen  
        this.anzahlSeerosenAlsMakeUp = 1;   
    }  
    // Konstruktoren Ende  
    // --------------------------------
    // Methoden  
    public int getAnzahlSeerosen(){ 
        return anzahlSeerosenAlsMakeUp; 
    } 
    // Methoden Ende  
}  