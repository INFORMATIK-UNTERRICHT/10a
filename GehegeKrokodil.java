class GehegeKrokodil implements Gehege{
    // Attribute
    private int anzahlEnthaltenerTiere; // Standartdatentyp
    private Krokodil hans; // Referenzattribut -> Objekt
    private Krokodil rudi;
    private Krokodil petra;   
    // Attribute Ende 
    // --------------------------------
    // Konstruktoren   
    public GehegeKrokodil(){
        this.hans = new Krokodil("männlich");
        this.rudi = new Krokodil("männlich");
        this.petra = new Krokodil("weiblich");
        this.anzahlEnthaltenerTiere = 3;
    }
    // Konstruktoren Ende
    // --------------------------------
    // Methoden
    public void fuettern(){
        this.rudi.fresse();
        this.hans.fresse();
        this.petra.fresse();       
    }
    
    public void informationenAusgeben(){
        // NICHT System.out.println(hans)
        this.hans.gibInformationenAus();
        this.rudi.gibInformationenAus();
        this.petra.gibInformationenAus();
    }
    // Methoden Ende
}

