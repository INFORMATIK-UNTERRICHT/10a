public class KrokodilEnkel extends KrokodilKind { 
    // Attribute  
    private int anzahlKrokodilFreunde; 
    // Attribute Ende  
    // ------------------------------  
    // Konstruktoren 
    public KrokodilEnkel(){ 
        super();
        this.anzahlKrokodilFreunde = 0; 
    } 
    // Konstruktoren Ende  
    // ------------------------------  
    // Methoden  
    public void erhöheAnzahlFreundeUmEins(){  
        anzahlKrokodilFreunde = this.anzahlKrokodilFreunde + 1;  
    } 
    // Methoden Ende  
} 