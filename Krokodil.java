public class Krokodil implements Tier{
    // Attribute
    private int lebensenergie;
    private int anzahlGefresseneTiere;
    private String hautfarbe;
    private String geschlecht;
    // Attribute Ende
    // ------------------------------
    // Konstruktoren 
    public Krokodil(){
        this.lebensenergie=100;
        this.anzahlGefresseneTiere=0;
        this.hautfarbe="gruen";
        this.geschlecht = "männlich";
    }

    public Krokodil(int lebensenergiePar, int anzahlGefresseneTierePar){
        this.lebensenergie = lebensenergiePar;
        this.anzahlGefresseneTiere = anzahlGefresseneTierePar;
        this.hautfarbe="gruen";
        this.geschlecht = "männlich";
    }

    public Krokodil(String geschlechtPar){
        this.lebensenergie=100;
        this.anzahlGefresseneTiere=0;
        this.hautfarbe="gruen";
        this.geschlecht = geschlechtPar;
    }
    // Konstruktoren Ende
    // Methoden
    public void setLebensenergie(int lebensenergiePar) {
        this.lebensenergie = lebensenergiePar;
    }

    public void setAnzahlGefresseneTiere(int anzahlGefresseneTierePar) {
        this.anzahlGefresseneTiere = anzahlGefresseneTierePar;
    }

    public int getLebensenergie() {
        return lebensenergie;
    }

    public int getAnzahlGefresseneTiere() {
        return anzahlGefresseneTiere;
    }

    public String getHautfarbe() {
        return hautfarbe;
    }

    public void gibInformationenAus() {
        System.out.println("Lebensenergie: " + lebensenergie);
        System.err.println("Anzahl gefressene Tiere: " + anzahlGefresseneTiere);;
        System.out.println("Hautfarbe: " + hautfarbe);
    }

    public void fresse(){
         this.setLebensenergie(lebensenergie + 20);
         this.setAnzahlGefresseneTiere(anzahlGefresseneTiere + 1);
    } 

    // Methoden Ende
}
